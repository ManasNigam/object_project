const mapObjectFunction = require("../mapObject.js")

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

cb = function (value) {
   return value;
}

console.log(mapObjectFunction(testObject, cb));
