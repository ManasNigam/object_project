const mapObjectFunction = (obj, cb) => {
    const mappedObject = {}
    for (let value in obj) {
        mappedObject[value] = cb((obj[value]))
    }
    return mappedObject;
}

module.exports = mapObjectFunction