const getKeys = require("./keys.js")
const getValues = require("./values.js")


const invertFunction = (testObject) => {
    const keys = getKeys(testObject)
    const values = getValues(testObject)
    let invertedObject = {}
    for (let i = 0; i < keys.length; i++) {
        invertedObject[values[i]] = keys[i]
    }

    return invertedObject
}


module.exports = invertFunction