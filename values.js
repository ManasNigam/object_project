const valueFunction = (obj) => {
    let allValues = []
    for (const values in obj) {
        if ((typeof obj[values]) === 'function') {
            continue;
        } else {
            allValues.push(obj[values])
        }
    }
    return allValues
}
module.exports = valueFunction